<?php
/**
 * @package n3tComingSoon
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2010 - 2013 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// No _JEXEC test, as this file is called directly, not through Joomla API
// defined( '_JEXEC' ) or die( 'Restricted access' );

header( 'Content-type: text/x-component' );
include( 'PIE.htc' );
?>