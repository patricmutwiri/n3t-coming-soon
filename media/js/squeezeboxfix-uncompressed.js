window.addEvent("domready", function(){
  SqueezeBox.handlers.extend({
    ajax: function(url) {
      var options = this.options.ajaxOptions || {};
      this.asset = new Request.HTML(Object.merge({
         method: 'get',
         evalScripts: false,
            onSuccess: function() {
            this.applyContent(this.asset.response.html);
            if (options.evalScripts !== null && options.evalScripts) Browser.exec(this.asset.response.javascript);
            this.fireEvent('onAjax', [this.asset.response.html, this.asset]);
            this.asset = null;
         }.bind(this),
         onFailure: this.onError.bind(this)
      }, this.options.ajaxOptions));
      this.asset.send.delay(10, this.asset, [{url: url}]);
    }
  });
});